import React, { Fragment } from "react";
import NavBar from "../components/NavBar";
import SideBar from "../components/SideBar";

const Home = () => {
  return (
    <Fragment>
    <div className="sidebar-mini">
      <div className="wrapper">
        <NavBar />
      </div>
    </div>
    <SideBar />
    </Fragment>
  );
};

export default Home;
