import './App.css';
import { Fragment } from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Login from './components/Login';
import Home from './pages/Home';

function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/login" exact element={<Login username="Nombre por defecto"/>}/>
          <Route path='/' exact element={<Home/>}/>
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
